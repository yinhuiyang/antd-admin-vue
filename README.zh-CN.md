
环境和依赖
----

- node
- yarn
- webpack
- eslint
- @vue/cli ~3



- 安装依赖
```
yarn install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

- Lints and fixes files
```
yarn run lint
```



